import axios from 'axios';
import { AUTH_USER, AUTH_ERROR } from './types';
const URL = 'https://guarded-thicket-22918.herokuapp.com';

export const login = (formProps, callback) => async dispatch => {
  try {
    const response = await axios.post(
      `${URL}/login`,
      formProps
    );

    dispatch({ type: AUTH_USER, payload: response.data.accessToken });
    localStorage.setItem('accessToken', response.data.accessToken);
    callback();
  } catch (e) {
    dispatch({ type: AUTH_ERROR, payload: e.response.data.error });
  }
};

export const logout = () => {
  localStorage.removeItem('accessToken');

  return {
    type: AUTH_USER,
    payload: ''
  };
};
