import React from 'react';
import { Link } from 'react-router-dom';

export default () => {
  return (
    <main className="container  container--centered">
      <h1 className="title">
        Welcome to the Monzo Developer Portal
      </h1>

      <div className="content">
        <p>
          Here be dragons.
        </p>
      </div>

      <Link to="/login" className="btn  btn--no-pad">
        Login
      </Link>
    </main>
  )
};
