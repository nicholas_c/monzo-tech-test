import React, { Component } from 'react';
import axios from 'axios';
import requireAuth from './requireAuth';
import { Link } from 'react-router-dom';

class AppUsers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      page: 1,
      isLoading: false
    }
  }

  fetchData() {
    axios.get(`https://guarded-thicket-22918.herokuapp.com/apps/${this.props.match.params.id}/users?offset=${(this.state.page * 25) - 25}`, {
      'headers': {
        'Authorization': this.props.auth
      }
    })
      .then(data => {
        if (data.status === 200) {
          this.setState({
            users: data.data.users,
            isLoading: false
          });
        }
      })
      .catch(() => {
        this.props.history.push('/logout')
      });
  }

  componentDidMount() {
    this.setState({
      isLoading: true
    });

    this.fetchData();
  }

  pagination(newPage) {
    this.state.page = newPage;
    this.fetchData();
  }

  render() {
    return (
      <div className="container">
        <div className="title">
          <h1>
            User list
          </h1>

          <Link to="/apps" className="btn">
            Back to app list
          </Link>
        </div>

        <div className="user__list">
          {this.state.users.map((el, i) => {
            return (
              <div className="user__list__item" key={el.id}>
                <img
                  src={el.avatar}
                  alt={el.name}
                  className="user__list__img"
                />

                <p>
                  {el.name}
                </p>
              </div>
            )
          })}
        </div>

        <div className="pagination">
          {
            this.state.page > 1 &&
            <a className="btn  pagination__link" onClick={() => { this.pagination(this.state.page - 1) } }>
              Previous
            </a>
          }

          {
            this.state.users.length === 25 &&
            <a className="btn  pagination__link" onClick={() => { this.pagination(this.state.page + 1) } }>
              Next
            </a>
          }
        </div>
      </div>
    )
  }
}

export default requireAuth(AppUsers);
