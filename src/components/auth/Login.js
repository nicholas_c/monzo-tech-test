import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { compose } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Login extends Component {
  onSubmit = formProps => {
    this.props.login(formProps, () => {
      this.props.history.push('/apps');
    });
  };

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="container">
        <form className="login" onSubmit={handleSubmit(this.onSubmit)}>
          <fieldset>
            <label htmlFor="email">Email</label>

            <Field
              id="email"
              name="email"
              type="email"
              component="input"
              autoComplete="none"
            />
          </fieldset>

          <fieldset>
            <label htmlFor="password">Password</label>

            <Field
              id="password"
              name="password"
              type="password"
              component="input"
              autoComplete="none"
            />
          </fieldset>

          <div className="login__error">{this.props.errorMessage}</div>

          <button className="btn">Login</button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { errorMessage: state.auth.errorMessage };
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'login' })
)(Login);
