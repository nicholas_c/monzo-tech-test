import React from 'react';
import PropTypes from 'prop-types';
import monzoLogo from '../assets/logo.svg';

class Logo extends React.PureComponent {
  render() {
    const { large } = this.props;

    return (
      <div className={(large ? 'logo--large' : 'logo')}>
        <img src={monzoLogo} alt="Monzo" className="logo__image" />
      </div>
    );
  }
}

Logo.defaultProps = {
  large: false,
};

Logo.propTypes = {
  large: PropTypes.bool,
};

export default Logo;
