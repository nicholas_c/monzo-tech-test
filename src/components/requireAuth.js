import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

export default ChildComponent => {
  class ComposedComponent extends Component {
    // Our component just got rendered
    componentDidMount() {
      this.shouldNavigateAway();
    }

    // Our component just got updated
    componentDidUpdate() {
      this.shouldNavigateAway();
    }

    shouldNavigateAway() {
      if (!this.props.auth) {
        this.props.history.push('/');
      } else {
        axios.get(`https://guarded-thicket-22918.herokuapp.com/`, {
          'headers': {
            'Authorization': this.props.auth
          }
        })
          .catch(() => {
            this.props.history.push('/logout');
          })
      }
    }

    render() {
      return <ChildComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { auth: state.auth.authenticated };
  }

  return connect(mapStateToProps)(ComposedComponent);
};
