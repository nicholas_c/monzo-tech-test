import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Logo from './Logo';

class Header extends Component {
  renderLinks() {
    if (this.props.authenticated) {
      return (
        <div className="header__tagline">
          <Link className="btn" to="/apps">My Apps</Link>
          <Link className="btn" to="/logout">Logout</Link>
        </div>
      );
    } else {
      return (
        <div className="header__tagline">
          <Link className="btn" to="/login">Login</Link>
        </div>
      );
    }
  }

  toggleNav(e) {
    e.target.classList.toggle('is-active')
  }

  render() {
    return (
      <header className="header">
        <div className="header__container">
          <Link to="/">
            <Logo />
          </Link>

          <div className="header__mob-nav" onClick={this.toggleNav}>
            <span></span>
          </div>

          {this.renderLinks()}
        </div>
      </header>
    );
  }
}

function mapStateToProps(state) {
  return { authenticated: state.auth.authenticated };
}

export default connect(mapStateToProps)(Header);
