import React, { Component } from 'react';
import axios from 'axios';
import requireAuth from './requireAuth';
import { Link } from 'react-router-dom';
import TimeAgo from 'react-timeago'

class Apps extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apps: [],
      isLoading: false,
    }
  }

  fetchData() {
    axios.get(`https://guarded-thicket-22918.herokuapp.com/apps`, {
      'headers': {
        'Authorization': this.props.auth
      }
    })
      .then(data => {
        if (data.status === 200) {
          this.setState({
            apps: data.data.apps,
            isLoading: false
          });
        }
      })
      .catch(() => {
        this.props.history.push('/logout')
      });
  }

  getEditLink(id) {
    return `apps/${id}/edit`;
  }

  getUsersLink(id) {
    return `apps/${id}/users`;
  }

  componentDidMount() {
    this.setState({ isLoading: true });

    this.fetchData()
  }

  render() {
    return (
      <div className="app  container">
        <div className="title">
          <h1>
            My Applications
          </h1>

          <button disabled title="There's no endpoint for this :(" className="btn">
            New app
          </button>
        </div>

        <div className="app__list">
          {this.state.apps.map((el, i) => {
            return (
              <div className="app__list__item" key={el.id}>
                <div className="app__list__item__content">
                  <img
                    src={el.logo + '?' + el.id}
                    alt={el.name}
                    className="app__list__img"
                  />

                  <p>
                    {el.name}<br />
                    <span><TimeAgo date={el.created} /></span>
                  </p>
                </div>

                <div className="app__list__item__content">
                  <Link className="btn" to={this.getEditLink(el.id)}>
                    Edit Application
                  </Link>

                  <Link className="btn" to={this.getUsersLink(el.id)}>
                    View Users
                  </Link>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}

export default requireAuth(Apps);
