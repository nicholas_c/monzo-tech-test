import React, { Component } from 'react';
import axios from 'axios';
import requireAuth from './requireAuth';
import { Link } from 'react-router-dom';

class AppUsers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      app: {
        id: '',
        created: '',
        name: '',
        logo: ''
      },
      users: [],
      page: 1,
      isLoading: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  fetchData() {
    axios.get(`https://guarded-thicket-22918.herokuapp.com/apps`, {
      'headers': {
        'Authorization': this.props.auth
      }
    })
      .then(data => {
        if (data.status === 200) {
          for (let i = 0; i < data.data.apps.length; i++) {
            if (data.data.apps[i].id === this.props.match.params.id) {
              this.setState({
                app: data.data.apps[i]
              })
            }
          }

          if (Object.keys(this.state.app).length === 0) {
            this.props.history.push('/apps');
          }
        }
      })
      .catch(e => {
        console.log(e);
      })
  }

  componentDidMount() {
    this.setState({
      isLoading: true
    });

    this.fetchData();
  }

  handleChange(e) {
    const app = this.state.app;

    app[e.target.getAttribute('name')] = e.target.value

    this.setState({
      app: app
    });
  }

  handleSubmit(e){
    e.preventDefault();

    const options = {
      method: 'PUT',
      headers: {
        'Authorization': this.props.auth,
        'content-type': 'application/json'
      },
      data: this.state.app,
      url: `https://guarded-thicket-22918.herokuapp.com/apps/${this.props.match.params.id}`
    };
    axios(options)
      .then(data => {
        this.props.history.push('/apps');
      })
      .catch(e => {
        this.props.history.push('/logout');
      })
  }

  render() {
    return (
      <div className="container">
        <div className="title">
          <h1>
            Edit Application
          </h1>

          <Link to="/apps" className="btn">
            Back to app list
          </Link>
        </div>

        <form onSubmit={this.handleSubmit}>
          <fieldset>
            <label htmlFor="id">
              ID
            </label>

            <input type="text" id="id" name="id" readOnly value={this.state.app.id} />
          </fieldset>

          <fieldset>
            <label htmlFor="created">
              Created
            </label>

            <input type="text" id="created" name="created" readOnly value={this.state.app.created} />
          </fieldset>

          <fieldset>
            <label htmlFor="name">
              Name
            </label>

            <input type="text" id="name" name="name" onChange={this.handleChange} value={this.state.app.name} />
          </fieldset>

          <fieldset>
            <label htmlFor="logo">
              Logo
            </label>

            <input type="text" id="logo" name="logo" onChange={this.handleChange} value={this.state.app.logo} />
          </fieldset>

          <fieldset>
            <button className="btn" type="submit">
              Save
            </button>
          </fieldset>
        </form>
      </div>
    )
  }
}

export default requireAuth(AppUsers);
