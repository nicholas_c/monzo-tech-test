import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';

import reducers from './reducers';
import App from './components/App';
import Welcome from './components/Welcome';
import Apps from './components/Apps';
import AppUsers from './components/AppUsers';
import AppEdit from './components/AppEdit';
import Logout from './components/auth/Logout';
import Login from './components/auth/Login';
import './index.css';

const store = createStore(
  reducers,
  {
    auth: { authenticated: localStorage.getItem('accessToken') }
  },
  applyMiddleware(reduxThunk)
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App>
        <Route path="/" exact component={Welcome} />
        <Route path="/apps" exact component={Apps} />
        <Route path="/apps/:id/users" component={AppUsers} />
        <Route path="/apps/:id/edit" component={AppEdit} />
        <Route path="/logout" component={Logout} />
        <Route path="/login" component={Login} />
      </App>
    </BrowserRouter>
  </Provider>,
  document.querySelector('#root')
);
