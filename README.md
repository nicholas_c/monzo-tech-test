# Monzo Developer Portal // Tech Test

## URL
https://nicholas-c-monzo-test.herokuapp.com/

## Future FE and API improvements
### Desired FE changes
Given time contraints of doing this build within 4 hrs, building a proper architecture for the CSS using a BEMIT structure (Looking at monzo.com, BEMIT has been used there for the main site and something I am familiar with from experience at Propeller) was slightly out of scope. That said components have been aptly named using BEM where possible and more details about the FE can be found below.

### Desired API endpoints
End point to allow fetching the information of a single app - Would be useful for the edit and user pages to fetch the data for a single app without fetching them all and getting just the one you need.
End point to allow new apps - For now there is a button in place of where I think it'd sit, however there is no endpoint for this.

## Auth & protected pages
Authorisation is handled in an action and reducer by Redux to save the state and access it across the board. Each of the protected routes carries out a check to verify the Auth token is still alive - If it is not it will force a logout. In the ideal world with more time outside of the 4hrs given for the task this should try and reauth and get a new token, however that is not the case for now.

## Routes:
Routing and state managment is maintained by Redux and React-router. As mentioned above, any protected view will check the API for a valid Auth token. (/src/components/requireAuth.js)

	/ - Welcome page
	/login - Login page with form, handles incorrect email/password and shows error message
	/logout - Kills the session and redirects to the welcome page
	/apps - *Protected view* - Lists all apps fetched from the API
	/apps/:id/edit - *Protected view* - Basic form allowing editing of fields on the app. Redirects to /apps on success.
	/apps/:id/users - *Protected view* - Paginated list of users - Shows 25 at a time.

## Frontend and CSS
*Note - I'd like to do some architecture changes here*
*Note* - Global CSS was used as opposed to CSS in components to utilize PostCSS for speed for this task.
PostCSS has been used to generate a global CSS file for this project (With PostCSS-Nesting, replicating Sass style functionality), everything is fully responsive with the header having a "mobile navigation" to show/hide basic navigation items.
